'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
  Image,
  StyleSheet,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

class SplashScreen extends Component {

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <ImageBackground style={styles.splashMsgContainer} source={require('./images/background.jpg')}>

        <Icon name="check-circle" color="white" size={100}/>
        <Text style={{
          fontSize: 30,
          fontWeight: 'bold',
          color: 'white',
          marginTop: 20
        }}>STRING<Text style={{
          fontSize: 20,
          color: 'white'
        }}>checker</Text>
        </Text>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  splashMsgContainer: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

module.exports = SplashScreen;