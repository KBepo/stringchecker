'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
  Image,
  StyleSheet,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

class MainPage extends Component {

  static navigationOptions = {
    header: null
  };

  state = {
    inputString: ''
  }

  checkIfFormatIsValid = () => {
    if(this.state.inputString.length < 8){
      return false
    }

    if(this.state.inputString.toLowerCase() == this.state.inputString){
      return false
    }

    if(!/\d/.test(this.state.inputString)){
      return false
    }

    let splitStringByNumbers = this.state.inputString.split(/([0-9]+)/)
    let foundValidatingError = false
    splitStringByNumbers.map((array, index)=>{
      if(index == 0 && /\d/.test(array)){
        foundValidatingError = true
      }else if(index == 0 && array.length<2){
        foundValidatingError = true
      }else if(index !=0 && index != splitStringByNumbers.length-1){
        if(!/\d/.test(array) && array.length<2){
          foundValidatingError = true
        }
      }else if(index == splitStringByNumbers.length-1 && /\d/.test(array)){
        foundValidatingError = true
      }else if(index == splitStringByNumbers.length-1 && array.length<2){
        foundValidatingError = true
      }
    })

    if(foundValidatingError){
      return false
    }

    return true
  }

  render() {
    return (
      <ImageBackground style={styles.splashMsgContainer} source={require('./images/background.jpg')}>

        <View style={{height: 100, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
          <Icon name="check-circle" color="white" size={50}/>
          <Text style={{
            fontSize: 20,
            fontWeight: 'bold',
            color: 'white'
          }}>STRING<Text style={{
            fontSize: 15,
            color: 'white'
          }}>checker</Text>
          </Text>
        </View>

        <View style={{flex: 1, padding: 40, position: 'relative'}}>
          <View style={{marginTop: 50, marginBottom: 20}}>
            {
              this.state.inputString
              ?
              <View style={{height: 20, justifyContent: 'flex-end'}}>
                <Text style={{color: 'white', textAlign: 'center', fontSize: 12}}>Upišite tekst za provjeru</Text>
              </View>
              :
              <View style={{height: 20}}></View>
            }
            <TextInput
              style={{height: 50, color: 'white', justifyContent: 'center', fontSize: 20}}
              onChangeText={(text) => this.setState({inputString:text.replace(/\s/g, '')})}
              value={this.state.inputString}
              placeholder="Upišite tekst za provjeru"
              underlineColorAndroid={'white'}
              placeholderTextColor={'rgba(255,255,255,0.7)'}
              selectionColor={'white'}
              textAlign={'center'}
            />
          </View>
          <TouchableOpacity
          onPress={()=>{
            Alert.alert(
              'Pravila za tekst:',
              '- mora sadržavati barem 8 znakova \n- mora sadržavati barem jedno veliko slovo \n- broj mora dijeliti niz u barem dva niza znakova'
            )
          }}
          style={{
            position: 'absolute',
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            width: 40,
            right: 0,
            top: 110
          }}>
            <Icon color="white" size={20} name="info-circle" />
          </TouchableOpacity>

          {
            this.state.inputString
            ?
            <View>
              <Text style={{textAlign: 'center', color: 'white'}}>
                Format teksta je:
              </Text>
              {
                this.checkIfFormatIsValid()
                ?
                <Text style={{textAlign: 'center', color: '#006400', fontSize: 20}}>
                  ISPRAVAN
                </Text>
                :
                <Text style={{textAlign: 'center', color: '#8B0000', fontSize: 20}}>
                  NEISPRAVAN
                </Text>
              }
            </View>
            :
            null
          }
          
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  splashMsgContainer: {
    width: '100%',
    height: '100%'
  }
});

module.exports = MainPage;