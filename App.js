import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation';

var SplashScreen = require('./SplashScreen.js');
var MainPage = require('./MainPage.js');

const routesConfig = {
  Splash: { screen: SplashScreen },
  Main: { screen: MainPage }
}

const StackNavigation = createStackNavigator(routesConfig, {initialRouteName: 'Splash'})
const MainStackNavigator = createAppContainer(StackNavigation);
export default class App extends Component<Props> {
  componentDidMount(){
    setTimeout(function(){
      this.refs.stackNavigation._navigation.replace({
        routeName: 'Main'
      })
    }.bind(this), 2000)
  }

  render() {
    return (
      <MainStackNavigator
        ref="stackNavigation"
      />
    );
  }
}